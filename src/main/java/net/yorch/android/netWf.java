package net.yorch.android;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * netWf<br>
 *
 * netWf Android Client<br><br>
 *
 * Copyright 2016 Jorge Alberto Ponce Turrubiates
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @version    1.0, 2016-02-08
 * @author     <a href="mailto:the.yorch@gmail.com">Jorge Alberto Ponce Turrubiates</a>
 */
public class netWf {
	/**
	 * WebService URL
	 */
	private String wsUrl = "";
	
	/**
	 * WebService Key
	 */
	private String wsKey = "";
	
	/**
	 * Create New netWf
	 * 
	 * @param url String WebService URL
	 * @param authKey String WebService Key
	 */
	public netWf(String url, String authKey) {
		wsUrl = url;
		wsKey = authKey;
	}
	
	/**
	 * Gets Table Rows with limit
	 * 
	 * @param name String Table Name
	 * @param limit int Limit of Rows
	 * @return JSONObject
	 */
	public JSONObject table(String name, int limit) {
		HttpClient httpclient = HttpClientBuilder.create().build();
        
        JSONObject retValue = new JSONObject();
        
        String url = wsUrl + "/api/table/" + name + "/" + String.valueOf(limit) + "?auth=" + wsKey;
        
        try{
            HttpGet httpget = new HttpGet(url);
            HttpResponse httpResponse = httpclient.execute(httpget);

            BufferedReader rd = new BufferedReader(new InputStreamReader(httpResponse.getEntity().getContent()));
            int status = httpResponse.getStatusLine().getStatusCode();
            StringBuffer response = new StringBuffer();
            
            if (status == 200) {
                String line = "";
           
                while ((line = rd.readLine()) != null)
                    response.append(line);
            }
            
            retValue =  new JSONObject(response.toString());
        }
        catch (IOException|JSONException e){
            retValue = new JSONObject();
        }
        
        httpclient = null;
        
		return retValue;
	}
	
	/**
	 * Gets Row by Id
	 * 
	 * @param name String Table Name
	 * @param fieldKey String Field Key
	 * @param key long Key Value
	 * @return JSONObject
	 */
	public JSONObject tableById(String name, String fieldKey, long key) {
		HttpClient httpclient = HttpClientBuilder.create().build();
        
        JSONObject retValue = new JSONObject();
        
        String url = wsUrl + "/api/tablebyid/" + name + "/" + String.valueOf(fieldKey) + "/" + key + "?auth=" + wsKey;
        
        try{
            HttpGet httpget = new HttpGet(url);
            HttpResponse httpResponse = httpclient.execute(httpget);

            BufferedReader rd = new BufferedReader(new InputStreamReader(httpResponse.getEntity().getContent()));
            int status = httpResponse.getStatusLine().getStatusCode();
            StringBuffer response = new StringBuffer();
            
            if (status == 200) {
                String line = "";
           
                while ((line = rd.readLine()) != null)
                    response.append(line);
            }
            
            retValue =  new JSONObject(response.toString());
        }
        catch (IOException|JSONException e){
            retValue = new JSONObject();
        }
        
        httpclient = null;
        
        return retValue;
	}
	
	/**
	 * Gets First Row of Table
	 * 
	 * @param table String Table Name
	 * @param fieldKey String Field Key
	 * @param where String Where Condition or * for not Where
	 * @return JSONObject
	 */
	public JSONObject first(String table, String fieldKey, String where) {
		HttpClient httpclient = HttpClientBuilder.create().build();
        
        JSONObject retValue = new JSONObject();
        
        String url = wsUrl + "/api/first/" + table + "/" + String.valueOf(fieldKey) + "/0/" + where + "?auth=" + wsKey;
        
        try{
            HttpGet httpget = new HttpGet(url);
            HttpResponse httpResponse = httpclient.execute(httpget);

            BufferedReader rd = new BufferedReader(new InputStreamReader(httpResponse.getEntity().getContent()));
            int status = httpResponse.getStatusLine().getStatusCode();
            StringBuffer response = new StringBuffer();
            
            if (status == 200) {
                String line = "";
           
                while ((line = rd.readLine()) != null)
                    response.append(line);
            }
            
            retValue =  new JSONObject(response.toString());
        }
        catch (IOException|JSONException e){
            retValue = new JSONObject();
        }
        
        httpclient = null;
        
        return retValue;
	}
	
	/**
	 * Gets Previous Row of Table
	 * 
	 * @param table String Table Name
	 * @param fieldKey String Field Key
	 * @param key long Key Value
	 * @param where String Where Condition or * for not Where
	 * @return JSONObject
	 */
	public JSONObject previous(String table, String fieldKey, long key, String where) {
		HttpClient httpclient = HttpClientBuilder.create().build();
        
        JSONObject retValue = new JSONObject();
        
        
        String url = wsUrl + "/api/previous/" + table + "/" + String.valueOf(fieldKey) + "/" + key + "/" + where + "?auth=" + wsKey;
        
        try{
            HttpGet httpget = new HttpGet(url);
            HttpResponse httpResponse = httpclient.execute(httpget);

            BufferedReader rd = new BufferedReader(new InputStreamReader(httpResponse.getEntity().getContent()));
            int status = httpResponse.getStatusLine().getStatusCode();
            StringBuffer response = new StringBuffer();
            
            if (status == 200) {
                String line = "";
           
                while ((line = rd.readLine()) != null)
                    response.append(line);
            }
            
            retValue =  new JSONObject(response.toString());
        }
        catch (IOException|JSONException e){
            retValue = new JSONObject();
        }
        
        httpclient = null;
        
        return retValue;
	}
	
	/**
	 * Gets Next Row of Table
	 * 
	 * @param table String Table Name
	 * @param fieldKey String Field Key
	 * @param key long Key Value
	 * @param where String Where Condition or * for not Where
	 * @return JSONObject
	 */
	public JSONObject next(String table, String fieldKey, long key, String where) {
		HttpClient httpclient = HttpClientBuilder.create().build();
        
        JSONObject retValue = new JSONObject();
        
        String url = wsUrl + "/api/next/" + table + "/" + String.valueOf(fieldKey) + "/" + key + "/" + where + "?auth=" + wsKey;
        
        try{
            HttpGet httpget = new HttpGet(url);
            HttpResponse httpResponse = httpclient.execute(httpget);

            BufferedReader rd = new BufferedReader(new InputStreamReader(httpResponse.getEntity().getContent()));
            int status = httpResponse.getStatusLine().getStatusCode();
            StringBuffer response = new StringBuffer();
            
            if (status == 200) {
                String line = "";
           
                while ((line = rd.readLine()) != null)
                    response.append(line);
            }
            
            retValue =  new JSONObject(response.toString());
        }
        catch (IOException|JSONException e){
            retValue = new JSONObject();
        }
        
        httpclient = null;
        
        return retValue;
	}
	
	/**
	 * Gets Last Row of Table
	 * 
	 * @param table String Table Name
	 * @param fieldKey String Field Key
	 * @param where String Where Condition or * for not Where
	 * @return JSONObject
	 */
	public JSONObject last(String table, String fieldKey, String where) {
		HttpClient httpclient = HttpClientBuilder.create().build();
        
        JSONObject retValue = new JSONObject();
        
        
        String url = wsUrl + "/api/last/" + table + "/" + String.valueOf(fieldKey) + "/0/"  + where + "?auth=" + wsKey;
        
        try{
            HttpGet httpget = new HttpGet(url);
            HttpResponse httpResponse = httpclient.execute(httpget);

            BufferedReader rd = new BufferedReader(new InputStreamReader(httpResponse.getEntity().getContent()));
            int status = httpResponse.getStatusLine().getStatusCode();
            StringBuffer response = new StringBuffer();
            
            if (status == 200) {
                String line = "";
           
                while ((line = rd.readLine()) != null)
                    response.append(line);
            }
            
            retValue =  new JSONObject(response.toString());
        }
        catch (IOException|JSONException e){
            retValue = new JSONObject();
        }
        
        httpclient = null;
        
        return retValue;
	}
	
	/**
	 * Execute Stored Procedure from netWf
	 * 
	 * @param procedure String Stored Procedure Name
	 * @param parameters JSONObject Parameters of Stored Procedure
	 * @return JSONObject
	 */
	public JSONObject procedure(String procedure, JSONObject parameters) {
		HttpClient httpclient = HttpClientBuilder.create().build();
        
        JSONObject retValue = new JSONObject();
        
        String url = wsUrl + "/api/procedure/" + procedure + "?auth=" + wsKey;
        
        try{
            HttpPost httppost = new HttpPost(url);
            StringEntity entJson = new StringEntity(parameters.toString());
            
            httppost.setEntity(entJson);
            
            HttpResponse httpResponse = httpclient.execute(httppost);

            BufferedReader rd = new BufferedReader(new InputStreamReader(httpResponse.getEntity().getContent()));
            int status = httpResponse.getStatusLine().getStatusCode();
            StringBuffer response = new StringBuffer();
            
            if (status == 200) {
                String line = "";
           
                while ((line = rd.readLine()) != null)
                    response.append(line);
            }
                        
            retValue =  new JSONObject(response.toString());
        }
        catch (IOException|JSONException e){
            retValue = new JSONObject();
        }
        
        httpclient = null;
        
        return retValue;
	}
	
	/**
	 * Execute Remote SQL Script from netWf
	 * 
	 * @param scriptName String SQL Script Name
	 * @param parameters JSONObject Parameters of Script
	 * @return JSONObject
	 */
	public JSONObject script(String scriptName, JSONObject parameters) {
		HttpClient httpclient = HttpClientBuilder.create().build();
        
        JSONObject retValue = new JSONObject();
        

        String url = wsUrl + "/api/script/" + scriptName + "?auth=" + wsKey;
        
        try{
            HttpPost httppost = new HttpPost(url);
            StringEntity entJson = new StringEntity(parameters.toString());
            
            httppost.setEntity(entJson);
            
            HttpResponse httpResponse = httpclient.execute(httppost);

            BufferedReader rd = new BufferedReader(new InputStreamReader(httpResponse.getEntity().getContent()));
            int status = httpResponse.getStatusLine().getStatusCode();
            StringBuffer response = new StringBuffer();
            
            if (status == 200) {
                String line = "";
           
                while ((line = rd.readLine()) != null)
                    response.append(line);
            }
                        
            retValue =  new JSONObject(response.toString());
        }
        catch (IOException|JSONException e){
            retValue = new JSONObject();
        }
        
        httpclient = null;
        
        return retValue;
	}
}
