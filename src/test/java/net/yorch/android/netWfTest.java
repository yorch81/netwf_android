package net.yorch.android;

import org.json.JSONArray;
import org.json.JSONObject;

import junit.framework.TestCase;

public class netWfTest extends TestCase {
	
	netWf nw;
	
	public netWfTest() {
		nw = new netWf("http://localhost:1080", "12345678987654321");
	}
	
	public void test_table() {
		int expected = 1;
		
		JSONObject jsonTable = nw.table("test", 1);
		
		assertEquals(expected, jsonTable.length());
	}
	
	public void test_first(){
		int expected = 1;
		
		JSONObject jsonTable = nw.first("test", "id", "*");
		
		assertEquals(expected , jsonTable.length());
	}
	
	public void test_previous(){
		int expected = 1;
		
		JSONObject jsonTable = nw.previous("test", "id",3,"*");
		
		assertEquals(expected , jsonTable.length());
	}
	
	public void test_next(){
		int expected = 1;
		
		JSONObject jsonTable = nw.next("test", "id",2,"*");
		
		assertEquals(expected , jsonTable.length());
	}
	
	public void test_last(){
		int expected = 1;
		
		JSONObject jsonTable = nw.last("test", "id","*");
		
		assertEquals(expected , jsonTable.length());
	}

	
	public void test_procedure() {
		int expected = 1;
		
		JSONObject jsonParam = new JSONObject();
		jsonParam.put("@number", 666);
		
		JSONObject jsonTable = nw.procedure("usp_test", jsonParam);
		
		assertEquals(expected, jsonTable.length());
	}
	
	public void test_script(){
		int expected = 0;
		
		JSONObject jsonParam = new JSONObject();
		jsonParam.put("@number", 666);
		
		JSONObject jsonTable = nw.script("script1.sql", jsonParam);
		
		JSONArray jRows = (JSONArray) jsonTable.get("Table");
				
		assertTrue(jRows.length() > expected);
	}
}
