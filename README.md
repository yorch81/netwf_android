# netWf 4 Android #

## Description ##
netDb Web Framework Android Client.

## Requirements ##
* [Java](https://www.java.com/es/download/)
* [Android](https://es.wikipedia.org/wiki/Android)

## Android Update ##
For android new versions, you must add the next line to build.gradle.

~~~

android {
    useLibrary 'org.apache.http.legacy'
}

~~~

After you must create a class and copy and paste the class code contains in the directory android.

## Developer Documentation ##
JavaDoc.

## Installation ##
Add dependency to build.gradle and sync.

~~~

compile 'net.yorch.android:netWf:1.0'

~~~

## Example ##
~~~

netWf nw = new netWf("URL_WEBSERVICE", "MASTER_KEY");

JSONObject jsonParam = new JSONObject();
jsonParam.put("@number", 666);

jsonTable = nw.procedure("usp_instest", jsonParam);

System.out.println(jsonTable.toString());

~~~

## Unit Test ##
To run unit test, creates database objects and run netDb Web Framework.

~~~

CREATE TABLE [dbo].[test](
	[id] [numeric](12, 0) IDENTITY(1,1) NOT NULL,
	[number] [numeric](12, 0) NOT NULL,
	[today] [datetime] NOT NULL CONSTRAINT [DF_test_today]  DEFAULT (getdate())
) ON [PRIMARY]

~~~

~~~

CREATE PROCEDURE [dbo].[usp_test] 
	@number NUMERIC(12,0) = 0
AS
BEGIN
	SET NOCOUNT ON;
	
	INSERT INTO dbo.test (number, today) VALUES (@number, GETDATE());
	
	SELECT 'OK' AS MSG;
END 

~~~

Add script1.sql to script directory in netDb Web Server.

~~~

SELECT * 
FROM MARKERS.DBO.test
WHERE number = @number

~~~

## References ##
https://www.java.com/

P.D. Let's go play !!!







